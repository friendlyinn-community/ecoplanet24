

local _evse = {}
evse_49 = _evse

local _L = {}

function _evse.get_status_register()
	return Modbus_Gate->EVSE_status_49->RegisterValue
end

_evse.time_loop_counter = 0

_L._cable_amperage = 0
_L.old_status_value = 'PWRINIT'
_L.ux_time_value = 0

function _L.status (v)
	Modbus_Gate->EV2_status = v or Modbus_Gate->EV2_status
	return Modbus_Gate->EV2_status
end

function _L.charging_time(v)
	Modbus_Gate->EV2_charging_time = v or Modbus_Gate->EV2_charging_time
	return Modbus_Gate->EV2_charging_time
end

function _L.charging_amperage(v)
	Modbus_Gate->EV2_charging_amperage = v or Modbus_Gate->EV2_charging_amperage
	return Modbus_Gate->EV2_charging_amperage
end

function _L.max_amperage(v)
	Modbus_Gate->EV2_max_amperage = v or Modbus_Gate->EV2_max_amperage
	return Modbus_Gate->EV2_max_amperage
end

function _L.authorization(v)
	Modbus_Gate->EV2_authorization = v or Modbus_Gate->EV2_authorization
	return Modbus_Gate->EV2_authorization
end

function _L.sleep_time(v)
	Modbus_Gate->EV2_sleep_minutes = v or Modbus_Gate->EV2_sleep_minutes
	return Modbus_Gate->EV2_sleep_minutes
end

function _L.eco_config(v)
	Modbus_Gate->EV2_eco_config = v or Modbus_Gate->EV2_eco_config
	return Modbus_Gate->EV2_eco_config
end

function _L.eco_start(v)
	Modbus_Gate->EV2_eco_start = v or Modbus_Gate->EV2_eco_start
	return Modbus_Gate->EV2_eco_start
end

function _L.eco_stop(v)
	Modbus_Gate->EV2_eco_stop = v or Modbus_Gate->EV2_eco_stop
	return Modbus_Gate->EV2_eco_stop
end

function _L.on_off(v)
	Modbus_Gate->EV2_on_off = v or Modbus_Gate->EV2_on_off
	return Modbus_Gate->EV2_on_off
end

function _L.cable_amperage(v)
	_L._cable_amperage = v or _L._cable_amperage
	return _L._cable_amperage
end

function _L.old_status(v)
	_L.old_status_value = v or _L.old_status_value
	return _L.old_status_value
end

function _L.ux_time(v) 
	return Modbus_Gate->LocalTime
end

function _L.charging_finish(v)
	Modbus_Gate->EV2_charging_finish = v or Modbus_Gate->EV2_charging_finish
	return Modbus_Gate->EV2_charging_finish
end

--------------------------------------------------------------------------------------------------
--- EVSE socket function 
--------------------------------------------------------------------------------------------------
_evse.parameter = {
	['status'] = nil,
	['charging_time'] = nil,
	['charging_amperage'] = nil,
    ['max_amperage'] = nil,
    ['authorization'] = nil,
    ['sleep_time'] = nil,
    ['eco_config'] = nil,
    ['eco_start'] = nil,
    ['eco_stop'] = nil,
    ['cable_amperage'] = nil,
    ['old_status'] = nil, 
    ['evse_time'] = nil
}

_evse.request_status = {}
for i, v in pairs(_evse.parameter) do
	_evse.request_status[i] = false
end

local function _set(value_name, value, mobile)
	print('_set(' .. value_name .. ')')
	
	if _evse.parameter[value_name] == nil then
		print('change')
		_L[value_name](value)
		_evse.parameter[value_name] = value
		_evse.request_status[value_name] = true
	end
	
	if mobile ~= nil then
		print('Mobile: ' .. tostring(value))
		
		_L[value_name](value)
	end
end


-- EVSE status function
--
function _evse.set_status(value)
	print('evse.set_status')
	_L.status(value)
end

_evse.get_status = _L.status

-- EVSE charging time
--
function _evse.set_charging_time(time)
	print ('_evse.set_charging_time: ' .. time)
	_L.charging_time(time)
end

_evse.get_charging_time = _L.charging_time

-- EVSE charging_amperage
--
function _evse.set_charging_amperage(amp, mobile)
	_L.charging_amperage(amp)
	
	if mobile ~= nil then
		_evse.parameter['charging_amperage'] = amp
	end
end

_evse.get_charging_amperage = _L.charging_amperage

-- EVSE max and current amperage
--
function _evse.set_max_amperage(value, mobile)
	_set('max_amperage', value, mobile)
end

_evse.get_max_amperage = _L.max_amperage

-- EVSE Eco function
--
function _evse.set_eco_config(value, mobile)
	_set('eco_config', value, mobile)
end

_evse.get_eco_config = _L.eco_config

function _evse.set_eco_start(hour, min, mobile)
	print('set_eco_start')
	_set('eco_start', string.format('%02i:%02i', hour, min), mobile)
end

_evse.get_eco_start = _L.eco_start


function _evse.set_eco_stop(hour, min, mobile)
	print('set_eco_stop: hour: ' .. tostring(hour) .. ' min: ' .. tostring(min))
	print('hour type: ' .. type(hour))
	local time = string.format('%02i:%02i', hour, min)
	
	print('Time: ' .. time)
	
	_set('eco_stop', string.format('%02i:%02i', hour, min), mobile)
end

_evse.get_eco_stop = _L.eco_stop

-- EVSE athorization
--
function _evse.set_authorization(value, mobile)
	_set('authorization', value, mobile)
end

_evse.get_authorization = _L.authorization

-- EVSE sleep time secounds
--
function _evse.set_sleep_time(value, mobile)
	_set('sleep_time', value, mobile)
end

_evse.get_sleep_time = _L.sleep_time

-- EVSE On/Off
function _evse.set_on_off(v)
	_L.on_off(v)
end

_evse.get_on_off = _L.on_off

function _evse.set_cable_amperage(value, mobile)
	_L.cable_amperage(value)
end

function _evse.set_cable_amperage_read()
	_evse.parameter['old_status'] = nil
	_evse.request_status['old_status'] = false
end

function _evse.get_cable_amperage()
	return _L.cable_amperage()
end

function _evse.set_old_status(value, mobile)
	_L.old_status(value)
end

_evse.old_status = _L.old_status

function _evse.set_charging_finish(value, mobile)
	_L.charging_finish(value)
end
--------------------------------------------------------------------------------------------
-- time synchronization

function _evse.evse_time()
end

function _evse.get_ux_time(value, mobile)
	return _L.ux_time()
end

--------------------------------------------------------------------------------------------
-- EVSE socket functions
--
_evse.fun = {
    ['charging_amperage'] = _evse.get_charging_amperage,
    ['max_amperage'] = _evse.get_max_amperage,
    ['authorization'] = _evse.get_authorization,
    ['sleep_time'] = _evse.get_sleep_time,
    ['eco_config'] = _evse.get_eco_config,
    ['eco_start'] = _evse.get_eco_start,
    ['eco_stop'] = _evse.get_eco_stop,
    ['cable_amperage'] = _evse.get_cable_amperage,
    ['evse_time'] = _evse.get_ux_time
}


print('_EVSE_49 initialize: Done')


return evse_49
