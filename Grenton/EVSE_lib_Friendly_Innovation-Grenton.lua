--[[
--
--
--
--]] 


if Evse_lib == nil then
    Evse_lib = {}
end

-- myGrenton handler function
--
if input_fun ~= nil and input_fun ~= 'default' then
    local f = Evse_lib.input_function[input_fun]
    if f ~= nil then
        f(input_param)
    end
    return
end

Evse = {}

------------------------------------------------------------------
-- register active socket - USER EDIT SECTION
--
Evse[48] = evse_48
Evse[49] = evse_49
-- Evse[50] = evse_50
-- Evse[51] = evse_51
--
-- end USER EDIT SECTION
-----------------------------------------------------------------

print('Initialize Evse_lib.')

-- EVSE charger status list
local status = {"PWRINIT", "READY", "CABLE", "CAR", "WAIT", "CHARGE", "CHCOOL", "CHARGED", "AUTH", "STOP", "CHANGEAMP",
                "DCS-ERROR", "DIODE_ERR", "CIR_ERR", "SOC-ERROR", "DC_TEST", "DC_ERR", "UPDATE"}

local TIME_COUNTER = 120

-- Debug function null code
-- Comment print command if not use
--
if mdbg == nil then
    mdbg = {}

    function mdbg.log(x, y)
        print('[Log]-', x, '-', y)
    end

    function mdbg.ERR(x, y)
        print('[ERR]-', x, '-', y)
    end

    function mdbg.lib(x, y)
        print('[Lib]-', x , '-', y)
    end
end

--------------------------------------------------------------------------------------------
-- Local library function
--------------------------------------------------------------------------------------------

-- Get / Set Evse RegisterNumber
--
local function request_evse(DeviceAddress, RegisterName, Value)
    local mbus_param = {}
    mbus_param = Mbus.init_param(mbus_param)

    mbus_param.RegisterName = RegisterName
    mbus_param.DeviceAddress = DeviceAddress
    mbus_param.RegisterAddress = Mbus.register_address[RegisterName]

    if Value ~= nil then
        mbus_param.AccessRight = 1 -- Read/Write
        mbus_param.Value = Value
    end
    -- print('Reguset Address' .. tostring(Mbus.register_address[RegisterName]))

	SYSTEM.Wait(500)
	
    Mbus.set_config(mbus_param)
end

-- convert time format to minutes
local function time2number(time)
	local poz = string.find(time, ":")
	
    return tonumber(string.sub(time, 1, poz - 1)), tonumber(string.sub(time, poz + 1))
end

local function time2string(time)
    local hour = math.floor(time / 60)
    local min = time - (hour * 60)

    return string.format('%02i:%02i', hour, min)
end

-------------------------------------------------------------------------------------------
-- EVSE status
--
function Evse_lib.status(nr_evse, stat)
    local new_status = status[stat]

    mdbg.lib('Evse_lib.status', 'status: ' .. tostring(new_status) .. '  Device address: ' .. tostring(nr_evse))
    if new_status ~= nil then
    	local old_status = Evse[nr_evse].get_status()
		if (new_status == 'CAR' or new_status == 'WAIT' or new_status == 'STOP') and (old_status == 'CHARGE' or old_status == 'CHCOOL') then
			Evse[nr_evse].set_charging_finish(1)
		end
		
    	-- Set status
        Evse[nr_evse].set_status(new_status)

		-- set charge power = 0
		if new_status == 'READY' or new_status == 'PWRINIT' then
			Evse[nr_evse].set_charging_amperage(0)
		end
		
        if new_status == 'CHARGE' or new_status == 'CHCOOL' then
            Evse[nr_evse].set_on_off(1)
        else
            Evse[nr_evse].set_on_off(0)
        end
        mdbg.lib('Evse_lib.status', 'Set EVSE status: ' .. new_status)
    else
        mdbg.ERR('Evse_lib.status', 'Bad EVSE status: ' .. tostring(stat))
    end
end

---------------------------------------------------------------------------------------------
-- EVSE charging time
--
function Evse_lib.charging_time(nr_evse, time)
    print('Evse_lib.set_charging_time: ' .. tostring(time))

    local hours = string.format('%0i', math.floor(time / 3600))
    local minutes = string.format("%0i", math.floor((time - tonumber(hours) * 3600) / 60))

    Evse[nr_evse].set_charging_time(hours .. ':' .. minutes)
end

---------------------------------------------------------------------------------------------
-- EVSE charging amperage
--
function Evse_lib.charging_amperage(nr_evse, amp)
    mdbg.log('Evse_lib.charging_amperage', 'Amperage: ' .. tostring(amp))

    Evse[nr_evse].set_charging_amperage(amp)
end

function Evse_lib.set_charging_amperage(nr_evse, amp)
    request_evse(nr_evse, 'set_charging_amperage', amp)
end

-----------------------------------------------------------------------------------------
-- EVSE max_amperage
--
function Evse_lib.max_amperage(nr_evse, amp)
    if (amp > 5 and amp <= 32) then
        Evse[nr_evse].set_max_amperage(amp)
    end
    mdbg.log('Evse_lib..set_max_amperage', 'Set max EVSE amperage: ' .. tostring(amp))
end

function Evse_lib.set_max_amperage(nr_evse, value)
    request_evse(nr_evse, 'set_max_amperage', value)
end

---------------------------------------------------------------------------------------------
-- EVSE Eco enable/disable
--
function Evse_lib.eco_config(nr_evse, config)
    local str_stat = 'Disable'

    if config == 1 then
        str_stat = 'Enable'
    end

    if config > 0 then
    	config = 1
        Evse[nr_evse].set_eco_config(config)
        mdbg.log('Evse_lib..set_eco_config', 'EVSE ECO config: ' .. str_stat)
    else
    	config = 0
    	Evse[nr_evse].set_eco_config(config)
        mdbg.log('Evse_lib..set_eco_config', 'EVSE ECO config: ' .. str_stat)
    end
end

function Evse_lib.set_eco_config(nr_evse, value)
    request_evse(nr_evse, 'set_eco_config', tonumber(value))
end

-- Eco start
--
function Evse_lib.eco_start(nr_evse, eco_time)
	print('eco_stop: ' .. tostring(eco_time))
    local hours = math.floor(eco_time / 60)
    local minutes = eco_time - (hours * 60)
	
	-- protect for bad hours and minutes
	print('Hour: ' .. tostring(hours) .. ' Min: ' .. tostring(minutes))
	if (hours < 0 and hours > 23) or (minutes < 0 or minutes > 59) then
		request_evse(nr_evse, 'set_eco_start', 22 * 60)
		return
	end
	
    Evse[nr_evse].set_eco_start(hours, minutes)
end

function Evse_lib.set_eco_start(nr_evse, value)
    local hour, min = time2number(value)

	-- protect for bad hours and minutes
	if (hour < 0 or hour > 23) or (min < 0 or min > 59) then
		request_evse(nr_evse, 'set_eco_stop', 22 * 60)
		mgdb.ERR('Evse_lib.set_eco_start', 'eco_start ERROR:' .. tostring(eco_time) .. ' - request_evse: set_eco_stop 22 * 60')
		return
	end
	
    request_evse(nr_evse, 'set_eco_start', hour * 60 + min)
end

-- Eco stop
--
function Evse_lib.eco_stop(nr_evse, eco_time)
    print('eco_stop: ' .. tostring(eco_time))

    local hour = math.floor(eco_time / 60)
    local min = eco_time - (hour * 60)

    print('Hour: ' .. tostring(hour) .. ' Min: ' .. tostring(min))
	if (hour < 0 or hour > 23) or (min < 0 or min > 59) then
		request_evse(nr_evse, 'set_eco_stop', 6 * 60)
		mgdb.ERR('Evse_lib.eco_stop', 'eco_stop ERROR:' .. tostring(eco_time) .. ' - request_evse: set_eco_stop 6 * 60')
		return
	end
	
    Evse[nr_evse].set_eco_stop(hour, min)
end

function Evse_lib.set_eco_stop(nr_evse, value)
    print('set_eco_stop - request: ' .. value)

    local hour, min = time2number(value)
    
    request_evse(nr_evse, 'set_eco_stop', hour * 60 + min)
end

----------------------------------------------------------------------------------
-- EVSE authorization
--
function Evse_lib.authorization(nr_evse, stat)
    print('Evse_lib.authorization: ', stat)

    if stat ~= nil or (stat == 1 or stat == 0) then
        Evse[nr_evse].set_authorization(stat)
    end
end

function Evse_lib.set_authorization(nr_evse, value)
    request_evse(nr_evse, 'set_authorization', tonumber(value))
end

----------------------------------------------------------------------------------
-- EVSE sleep
--
function Evse_lib.sleep_time(nr_evse, stat)
    if stat ~= nil or (stat == 1 or stat == 0) then
        Evse[nr_evse].set_sleep_time(stat)
    end
end

function Evse_lib.set_sleep_time(nr_evse, value)
    request_evse(nr_evse, 'set_sleep_time', tonumber(value))
end

----------------------------------------------------------------------------------
-- EVSE cable amperage
--
function Evse_lib.cable_amperage(nr_evse, val)
    Evse[nr_evse].set_cable_amperage(val)
end


----------------------------------------------------------------------------------
-- Evse time synchronization
--
function Evse_lib.evse_time(nr_evse, val)
	local local_time = Mbus.local_time()
	local step = 30
	
	if local_time - step > val or local_time + step < val then
		mdbg.lib('Evse_lib.evse_time', 'Zmiana czasu na EVSE: ', tostring(nr_evse) , ' EVSE: ', val , ' Local: ', local_time , 'roznica: ' .. (math.abs(val - local_time)))
		request_evse(nr_evse, 'set_evse_time', local_time)
		SYSTEM.Wait(500)
	else 
		print('Czas EVSE: ' .. tostring(val) , 'EVSE: ' .. tostring(nr_evse) , ' - Czas MBUS: ' .. tostring(local_time))
	end
	
end

function Evse_lib.set_evse_time(nr_evse, val)
end

----------------------------------------------------------------------------------
-- EVSE register functions
----------------------------------------------------------------------------------

-- local parameters
local _evse_number = 1
local _register_number = 0

local register_name = {
    [1] = 'charging_time',
    [2] = 'charging_amperage',
    [3] = 'max_amperage',
    [4] = 'authorization',
    [5] = 'sleep_time',
    [6] = 'eco_config',
    [7] = 'eco_start',
    [8] = 'eco_stop',
    [9] = 'cable_amperage',
    [10] = 'evse_time'
}

-- Read next device/register address
--
local function next_evse_register()
    local nr_evse = Mbus.evse_list[_evse_number]
	
    _register_number = _register_number + 1

	-- time loop
	if _register_number == 10 then 
		local nr = Mbus.evse_list[_evse_number]
		Evse[nr].time_loop_counter = Evse[nr].time_loop_counter + 1
		
		if Evse[nr].time_loop_counter < TIME_COUNTER then
			_register_number = _register_number + 1
		else
			Evse[nr].time_loop_counter = 0
		end
	end    
	
    if register_name[_register_number] == nil then -- end list EVSE register
        _register_number = 1
        _evse_number = _evse_number + 1
        if Mbus.evse_list[_evse_number] == nil then -- end list EVSE numbers
            _evse_number = 1
        end
        nr_evse = Mbus.evse_list[_evse_number]
    end

    -- print('nr_evse: ' .. tostring(nr_evse) .. ' register number: ' .. tostring(_register_number))
    return nr_evse, _register_number
end

function Evse_lib.mbus_read_register()
    local nr_evse, register = next_evse_register()

    while Evse[nr_evse].request_status[register_name[register]] == true do
        print('Skip: ' .. register_name[register] .. ' EV: '..  nr_evse)
        nr_evse, register = next_evse_register()
    end

    mdbg.lib('next_evse_register',
       "EV: " .. tonumber(nr_evse) .. ' Request number: ' .. tonumber(register) .. ' RegisterName: ' .. register_name[register])

    request_evse(nr_evse, register_name[register])
end

----------------------------------------------------------------------------------
-- EVSE write to the EVSE new value parameter if changed 
--
local function check_changed_param()
    local nr_evse = 0

    for i, nr in pairs(Mbus.evse_list) do
        nr_evse = nr

        for i, v in pairs(Evse[nr_evse].fun) do
            -- mdbg.lib('', 'Param : ' .. i)

            local val = v()
            if type(val) == 'boolean' then
                if val == true then
                    val = 1
                else
                    val = 0
                end
            end

            if Evse[nr_evse].parameter[i] ~= nil and Evse[nr_evse].parameter[i] ~= val then
                local old = Evse[nr_evse].parameter[i]
                if type(old) ~= 'string' then
                    old = tostring(old)
                end
                mdbg.lib('Evse_lib check_changed_param', 'param: ' .. i .. ' old value: ' .. old .. ' New value: ' .. tostring(val))

                local f = Evse_lib['set_' .. i]

                if f ~= nil then
                    mdbg.lib('check_changed_param', 'Execute Evse_lib[set_' .. i .. ']')
                    f(nr_evse, val)
                    return true
                end
            end
        end
    end

    return false
end

-- EVSE status handler
--
function Evse_lib.EVSE_status_handler(device)
	print('STATUS handler')
	
	local stat = Evse[device].get_status_register()
	
	Evse_lib.status(device, stat)
end

-- Handler read next device and/or register address
--
function Evse_lib.EVSE_handler(handler_type)
    local response = Mbus.Register()
	local handler_name = handler_type
	
    mdbg.lib('Evse_lib.EVSE_handler', 'type: ' .. handler_name .. ' Code: ' .. tostring(response.ErrorCode))

    print('MBUS handlr function. addres: ' .. tostring(response.RegisterAddress) .. ' - ' ..
              (tostring(response.RegisterValue) or 'Nil'))
    if response.ErrorCode ~= 0 then
        Evse_lib.mbus_read_register()
        return
    end

    if response.ErrorCode == 0 and string.sub(response.RegisterName, 1, 4) == 'set_' then
    	print('Request set: ' .. response.RegisterName)
    	
        -- format time to string HH:MM
        if response.RegisterName == 'set_eco_start' or response.RegisterName == 'set_eco_stop' then
            response.RegisterValue = time2string(response.RegisterValue)
        end

        Evse[response.DeviceAddress].parameter[string.sub(response.RegisterName, 5)] = response.RegisterValue
        Evse_lib.mbus_read_register()
        return
    end

    mdbg.lib('Evse.lib.EVSE_handler',
        'RegisterNume: ' .. response.RegisterName .. ' RegisterValue: ' .. tostring(response.RegisterValue))

    local fun = Evse_lib[response.RegisterName]

    if fun ~= nil and response.ErrorCode == 0 then
        fun(response.DeviceAddress, response.RegisterValue)
    else
        mdbg.ERR('Evse.lib.EVSE_handler', 'Function Nil addresse. RegisterName: ' .. (response.RegisterName or 'Nil'))
    end

    if check_changed_param() == false then
        mdbg.lib('Evse.lib.EVSE_handler', 'Next register: ')
        Evse_lib.mbus_read_register()
    end
end

-- RUN READ REGISTER loop
--
Evse_lib.mbus_read_register()

--------------------------------------------------------------------------------------
-- 																					--
--		EVSE for mobile myGrenton functions											--
--																					--
--------------------------------------------------------------------------------------
myG = {}

-- EVSE charging_amperage
--
function myG.charge_amp_inc(device)
    mdbg.lib('myG.charge_amp_inc', 'Device: ' .. tostring(device))

    local stat = Evse[device].get_status()
    if stat == 'CAR' or stat == 'CHARGE' or stat == 'CHCOOL' or stat == 'WAIT' or stat == 'AUTH' then
        local amp_cable = Evse[device].get_cable_amperage()
        local amp_max = Evse[device].get_max_amperage()
        local amp_charge = Evse[device].get_charging_amperage() + 1

        if amp_charge > amp_max or amp_charge > amp_cable then
        	print ('Return. New: ' .. tostring(amp_charge) .. ' Max: ' .. tostring(amp_max) .. ' Cable: ' .. tostring(amp_cable))
            return
        end

        Evse[device].set_charging_amperage(amp_charge, 'myG')
        request_evse(device, 'set_charging_amperage', amp_charge)
    end
end

function myG.charge_amp_dec(device)
    mdbg.lib('myG.charge_amp_dec', 'Device: ' .. tostring(device))

    local stat = Evse[device].get_status()
    if stat == 'CAR' or stat == 'CHARGE' or stat == 'CHCOOL' or stat == 'WAIT' or stat == 'AUTH' then
        local amp = Evse[device].get_charging_amperage() - 1

        if amp < 5 then
            return
        end

        Evse[device].set_charging_amperage(amp, 'myG')
        request_evse(device, 'set_charging_amperage', amp)
    end
end

-- EVSE max_amperage
--
function myG.max_amperage_inc(device)
    print('myG.max_amperage_inc')

    local amp = Evse[device].get_max_amperage() + 1

    if amp > 32 then
        return
    end
    Evse[device].set_max_amperage(amp, 'myG')
    request_evse(device, 'set_max_amperage', amp)
end

function myG.max_amperage_dec(device)
    print('myG.max_amperage_dec')

    local amp = Evse[device].get_max_amperage() - 1

    if amp < 5 then
        return
    end
    Evse[device].set_max_amperage(amp, 'myG')
    request_evse(device, 'set_max_amperage', amp)
end

-- EVSE authorization
--
function myG.authorization(device)
    local auth = Evse[device].get_authorization()
    if auth == 1 then
        auth = 0
    else
        auth = 1
    end
    Evse[device].set_authorization(auth, 'myG')
    request_evse(device, 'set_authorization', auth)
end

-- EVSE sleep_time
--
function myG.sleep_time_inc(device)
    mdbg.lib('myG.sleep_time_inc', 'Device: ' .. tostring(device))

    local sleep = Evse[device].get_sleep_time() + 1

    Evse[device].set_sleep_time(sleep, 'myG')
    request_evse(device, 'set_sleep_time', sleep)
end

function myG.sleep_time_dec(device)
    mdbg.lib('myG.sleep_time_dec', 'Device: ' .. tostring(device))

    local sleep = Evse[device].get_sleep_time() - 1
    if sleep < 0 then
        sleep = 0
    end
    Evse[device].set_sleep_time(sleep, 'myG')
    request_evse(device, 'set_sleep_time', sleep)
end

-- EVSE eco_config
--
function myG.eco_config(device)
    local eco = Evse[device].get_eco_config()
    if eco == 1 then
        eco = 0
    else
        eco = 1
    end

    Evse[device].set_eco_config(eco, 'myG')
    request_evse(device, 'set_eco_config', eco)
end

-- EVSE eco_start
--
function myG.eco_start_min(device)
    local hour, min = time2number(Evse[device].get_eco_start())

    min = min + 5
    if min >= 60 then
        min = 0
    end
    mdbg.lib('myG.eco_start_hour',
        'Dev: ' .. tostring(device) .. ' Hour: ' .. tostring(hour) .. ' Min: ' .. tostring(min))

    Evse[device].set_eco_start(hour, min, 'myG')
    request_evse(device, 'set_eco_start', hour * 60 + min)
end

function myG.eco_start_hour(device)
    local hour, min = time2number(Evse[device].get_eco_start())

    hour = hour + 1
    if hour >= 24 then
        hour = 0
    end
    mdbg.lib('myG.eco_start_hour',
        'Dev: ' .. tostring(device) .. ' Hour: ' .. tostring(hour) .. ' Min: ' .. tostring(min))

    Evse[device].set_eco_start(hour, min, 'myG')
    request_evse(device, 'set_eco_start', hour * 60 + min)
end

-- EVSE eco_stop
-- 
function myG.eco_stop_min(device)
    local hour, min = time2number(Evse[device].get_eco_stop())

    min = min + 5
    if min >= 60 then
        min = 0
    end

    Evse[device].set_eco_stop(hour, min, 'myG')
    request_evse(device, 'set_eco_stop', hour * 60 + min)
end

function myG.eco_stop_hour(device)
    local hour, min = time2number(Evse[device].get_eco_stop())

    hour = hour + 1
    if hour >= 24 then
        hour = 0
    end

    Evse[device].set_eco_stop(hour, min, 'myG')
    request_evse(device, 'set_eco_stop', hour * 60 + min)
end

-- EVSE get/set status
--
local function status_number(status_name)
    for i, v in pairs(status) do
        if v == status_name then
            return i
        end
    end

    return nil
end

function myG.charge_start(device)
	print('Charge START')
    mdbg.lib('myG.charge_start', 'Device:' .. tostring(device))
    local st = Evse[device].get_status()

    if st == 'WAIT' or st == 'AUTH' then
        request_evse(device, 'set_status', status_number('CHARGE'))
        Evse[device].set_on_off(1)
    end
end

function myG.charge_stop(device)
	print('Charge STOP')
    mdbg.lib('myG.charge_stop', 'Device: ' .. tostring(device))
    local st = Evse[device].get_status()

    if st == 'CHARGE' or st == 'CHCOOL' or st == 'AUTH' then
        print('STOP')
        request_evse(device, 'set_status', status_number('STOP'))
        Evse[device].set_on_off(0)
    end
end

Evse_lib.input_function = {
    ['EVSE'] = Evse_lib.EVSE_handler,
    ['EVSE_status'] = Evse_lib.EVSE_status_handler,
    ['mbus_read_register'] = Evse_lib.mbus_read_register,
    ['charge_amp_dec'] = myG.charge_amp_dec,
    ['charge_amp_inc'] = myG.charge_amp_inc,
    ['max_amperage_dec'] = myG.max_amperage_dec,
    ['max_amperage_inc'] = myG.max_amperage_inc,
    ['authorization'] = myG.authorization,
    ['sleep_time_dec'] = myG.sleep_time_dec,
    ['sleep_time_inc'] = myG.sleep_time_inc,
    ['eco_config'] = myG.eco_config,
    ['eco_start_min'] = myG.eco_start_min,
    ['eco_start_hour'] = myG.eco_start_hour,
    ['eco_stop_min'] = myG.eco_stop_min,
    ['eco_stop_hour'] = myG.eco_stop_hour,
    ['charge_start'] = myG.charge_start,
    ['charge_stop'] = myG.charge_stop
}

mdbg.lib('_EVSE_lib', 'EVSE library initialize ... Done')

return Evse_lib
