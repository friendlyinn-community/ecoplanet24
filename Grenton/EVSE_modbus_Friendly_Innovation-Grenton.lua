-----------------------------------------------------------------------------------------
-- ModBus RTU CLU register
Mbus = {}

-- EVSE addresse
local _EVSE_addresse = 48

local function _status_48()
	print("READ STATUS 48")
	return Modbus_Gate->EVSE_status_48->RegisterValue
end

-- EVSE charge Modbus RTU register
--
Mbus.register_address = {
	['status'] = 0,
	['charging_time'] 			= 101,	-- start Input registers
	['charging_amperage'] 		= 103,
	['max_amperage'] 			= 104,
	['authorization'] 			= 105,
	['sleep_time'] 				= 106,
	['eco_config'] 				= 107,
	['eco_start'] 				= 108,
	['eco_stop'] 				= 109,
	['cable_amperage']  		= 110,
	['evse_time']				= 116,
	['set_status'] 		    	= 200,	-- start Holding registers
	['set_charging_amperage'] 	= 203,
	['set_max_amperage'] 		= 204,
	['set_authorization'] 		= 205,
	['set_sleep_time'] 			= 206,
	['set_eco_config'] 			= 207,
	['set_eco_start'] 			= 208,
	['set_eco_stop'] 			= 209,
	['set_evse_time']			= 216
}

-- Set read/write two register data, 32 bits
Mbus.register_32bits = {
	['evse_time'] = 32,
	['set_evse_time'] = 32
}

-- default start parameters EVSE modbus request
Mbus.mbus_param = {
	['RegisterName'] = 'status',-- request name
	['DeviceAddress'] = 48,			-- default first socket address
	['AccessRight'] = 0,			-- 0 - Read / 1 - Read/Write
	['RegisterAddress'] = 100,		-- first holding register address, EVSE status register
	['TransmisionSpeed'] = 19200,	-- default EVSE transmision speed
	['ValueType'] = 1,				-- Number
	['BitPosition'] = 0,
	['BitCount'] = 16,
	['RefreshInterval'] = 1000,
	['ResponseTimeout'] = 600,
	['Divisor'] = 0,
	['Endianess'] = 1,				-- Swap bytes and words
	['RegisterType'] = 2,			-- Holding register
	['ErrorCode'] = nil,
	['Value'] = nil,
	['RegisterValue'] = nil,
	['StopBits'] = 0,
	['Parity'] = 0					-- NONE
}

Mbus.evse_list = {}

function Mbus.init_param(tParam)
	for i, v in pairs(Mbus.mbus_param) do
		tParam[i] = v
	end
	
	return tParam
end

Mbus.register_name = nil

local function create_evse_list()
	local ev_numbers = Modbus_Gate->EV_numbers
	local _start = 1
	local _end = string.find(ev_numbers, ",", _start)
	local i = 1

	--print('evse_list: ' .. ev_numbers)
	while _end ~= nil do
		Mbus.evse_list[i] = tonumber(string.sub(ev_numbers, _start, _end - 1))
		_start = _end + 1
		_end = string.find(ev_numbers, ",", _start)
		i = i + 1
	end

	-- last or single number
	Mbus.evse_list[i] = tonumber(string.sub(ev_numbers, _start, #ev_numbers))

	print('EVSE number: ' .. tostring(Mbus.evse_list[i]))
end

function Mbus.register_name(nr)
	for i, v in pairs(Mbus.register_address) do
		if v == nr then
			return i
		end
	end

	return 'Not found register name'
end

------------------------------------------------------------------------------------------
-- Modbus handler get/set function
--
function Mbus.set_config(_mbus_param)
	_mbus_param = _mbus_param or Mbus.mbus_param		-- initialize default parameters

	print('RegisterAddress: ' .. tostring(_mbus_param['RegisterAddress']))

	Mbus.register_name = _mbus_param.RegisterName

	Modbus_Gate->EVSE->SetRefreshInterval(0)
	
	-- 32 bits read/write register
	if Mbus.register_32bits[_mbus_param.RegisterName] ~= nil then
		Modbus_Gate->EVSE->SetBitCount(32)		-- read/write two register, 32 bits
	else
		Modbus_Gate->EVSE->SetBitCount(_mbus_param['BitCount'])
	end
	
	Modbus_Gate->EVSE->SetAccessRights(_mbus_param['AccessRight'])
	Modbus_Gate->EVSE->SetRegisterAddress(_mbus_param['RegisterAddress'])
	Modbus_Gate->EVSE->SetTransmisionSpeed(_mbus_param['TransmisionSpeed'])
	Modbus_Gate->EVSE->SetValueType(_mbus_param['ValueType'])
	Modbus_Gate->EVSE->SetBitPosition(_mbus_param['BitPosition'])
		
	Modbus_Gate->EVSE->SetReadWriteTimeout(_mbus_param['ResponseTimeout'])
	
	-- Write parameter to the EVSE
	if _mbus_param['Value'] ~= nil then
		Modbus_Gate->EVSE->SetValue(_mbus_param['Value'])
		Modbus_Gate->EVSE->SetRegisterType(_mbus_param['RegisterType'])
	else
		Modbus_Gate->EVSE->SetRegisterType(3)	-- Input Register
	end
	
	--[[
	Modbus_Gate->EVSE->SetRefreshInterval(_mbus_param['RefreshInterval'])
	Modbus_Gate->EVSE->SetDivisor(_mbus_param['Divisor'])
	Modbus_Gate->EVSE->SetEndianess(_mbus_param['Endianess'])
	Modbus_Gate->EVSE->SetRegisterType(_mbus_param['RegisterType'])
	Modbus_Gate->EVSE->SetValue(_mbus_param['Value'])
	Modbus_Gate->EVSE->SetStopBits(_mbus_param['StopBits'])
	Modbus_Gate->EVSE->SetParity(_mbus_param['Parity'])
	--]]

	Modbus_Gate->EVSE->SetDeviceAddress(_mbus_param['DeviceAddress'])
	Modbus_Gate->EVSE->SetRefreshInterval(_mbus_param['RefreshInterval'])
end

function Mbus.Register()
	local req = {
		['RegisterName'] = Mbus.register_name,
		['DeviceAddress'] = Modbus_Gate->EVSE->DeviceAddress,
		['RegisterAddress'] = Modbus_Gate->EVSE->RegisterAddress,
		['RegisterValue'] = Modbus_Gate->EVSE->RegisterValue,
		['ErrorCode'] = Modbus_Gate->EVSE->ErrorCode
	}
	return req
end

function Mbus.local_time()
	return Modbus_Gate->LocalTime
end

-- create list connected EVSE sockets
create_evse_list()

return Mbus
